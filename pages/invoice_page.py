"""
This module contains InvoiceDetailPage,
the page object for the Invoice Detail of automation-sandbox URL
"""

from selenium.webdriver.common.by import By
from random import choice

import yaml
import re

from pages.common import CommonPage


class InvoiceDetailPage(CommonPage):
    # Locators

    # Lists Locators

    # Input Locators

    # Button Locators
    BUTTON_LOGOUT = (By.CSS_SELECTOR, 'a.btn-outline-info')

    # Other Locators
    PAGE_TITLE = (By.CSS_SELECTOR, 'span.navbar-brand')
    PAGE_HEADER_TITLE = (By.CSS_SELECTOR, 'h2.mt-5')
    HOTEL_NAME_TITLE = (By.CSS_SELECTOR, 'h4.mt-5')
    INVOICE_NUMBER = (By.CSS_SELECTOR, 'h6.mt-2')
    INVOICE_DATE = (By.XPATH, '//span[text()="Invoice Date:"]/parent::li')
    DUE_DATE = (By.XPATH, '//span[text()="Due Date:"]/parent::li')
    BOOKING_CODE = (By.XPATH, '//td[preceding::td[text()="Booking Code"]]')
    ROOM_DESCRIPTION = (By.XPATH, '//td[preceding::td[text()="Room"]]')
    TOTAL_STAY_COUNT = (By.XPATH, '//td[preceding::td[text()="Total Stay Count"]]')
    TOTAL_STAY_AMOUNT = (By.XPATH, '//td[preceding::td[text()="Total Stay Amount"]]')
    CHECK_IN = (By.XPATH, '//td[preceding::td[text()="Check-In"]]')
    CHECK_OUT = (By.XPATH, '//td[preceding::td[text()="Check-Out"]]')
    CUSTOMER_DETAILS = (By.XPATH, '//div[preceding::h5[text()="Customer Details"]]')
    DEPOSIT_NOW = (By.XPATH, '//table[preceding::h5[text()="Billing Details"]]/tbody/tr/td[count(//td[text()="Deposit Nowt"]/preceding-sibling::*)+1]')
    TAX_AND_VAT = (By.XPATH, '//table[preceding::h5[text()="Billing Details"]]/tbody/tr/td[count(//td[text()="Tax&VAT"]/preceding-sibling::*)+1]')
    TOTAL_AMOUNT = (By.XPATH, '//table[preceding::h5[text()="Billing Details"]]/tbody/tr/td[count(//td[text()="Total Amount"]/preceding-sibling::*)+1]')


    # Initializer

    def __init__(self, browser):
        super().__init__(browser)

    # Interaction Element Methods

    def get_page_title(self):
        element = self.wait_visibility_of_element_located(self.PAGE_TITLE)
        value = element.text
        return value

    def get_header_title(self):
        element = self.wait_visibility_of_element_located(self.PAGE_HEADER_TITLE)
        value = element.text
        return value

    def get_hotel_name(self):
        value = self.browser.find_element(*self.HOTEL_NAME_TITLE).text
        return value

    def get_invoice_number(self):
        value = self.browser.find_element(*self.INVOICE_NUMBER).text
        value =''.join(filter(lambda i: i.isdigit(), value))
        return value

    def get_invoice_date(self):
        value = self.browser.find_element(*self.INVOICE_DATE).text
        value = value.replace("Invoice Date: ", "")
        return value

    def get_due_date(self):
        value = self.browser.find_element(*self.DUE_DATE).text
        value = value.replace("Due Date: ", "")
        return value

    def get_booking_code(self):
        value = self.browser.find_element(*self.BOOKING_CODE).text
        return value

    def get_room_description(self):
        value = self.browser.find_element(*self.ROOM_DESCRIPTION).text
        return value

    def get_total_stay_count(self):
        value = self.browser.find_element(*self.TOTAL_STAY_COUNT).text
        return value

    def get_total_stay_amount(self):
        value = self.browser.find_element(*self.TOTAL_STAY_AMOUNT).text
        return value

    def get_check_in(self):
        value = self.browser.find_element(*self.CHECK_IN).text
        return value

    def get_check_out(self):
        value = self.browser.find_element(*self.CHECK_OUT).text
        return value

    def get_customer_details(self):
        value = self.browser.find_element(*self.CUSTOMER_DETAILS).text
        value = value.replace('\n', ' ')
        return value

    def get_deposit_now(self):
        value = self.browser.find_element(*self.DEPOSIT_NOW).text
        return value

    def get_tax_and_vat(self):
        value = self.browser.find_element(*self.TAX_AND_VAT).text
        return value

    def get_total_amount(self):
        value = self.browser.find_element(*self.TOTAL_AMOUNT).text
        return value

    # Interaction Action Methods

    def assert_invoice_data(self, item):
        file_path = "pages\\data\\invoice_item_" + str(item) + ".yaml"
        with open(file_path, 'r') as file:
            invoice_data = yaml.load(file, Loader=yaml.FullLoader)
            assert self.get_hotel_name() == invoice_data["hotel_name"]
            assert self.get_invoice_number() == invoice_data["invoice_number"]
            assert self.get_invoice_date() == invoice_data["invoice_date"]
            assert self.get_due_date() == invoice_data["due_date"]
            assert self.get_booking_code() == invoice_data["booking_code"]
            assert self.get_room_description() == invoice_data["room"]
            assert self.get_total_stay_count() == invoice_data["total_stay_count"]
            assert self.get_total_stay_amount() == invoice_data["total_stay_amount"]
            assert self.get_check_in() == invoice_data["check_in"]
            assert self.get_check_out() == invoice_data["check_out"]
            assert self.get_customer_details() == invoice_data["customer_details"]
            assert self.get_deposit_now() == invoice_data["deposit_now"]
            assert self.get_tax_and_vat() == invoice_data["tax_and_vat"]
            assert self.get_total_amount() == invoice_data["total_amount"]

