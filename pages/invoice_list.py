"""
This module contains InvoiceListPage,
the page object for the Invoice List of automation-sandbox URL
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from random import choice

from pages.common import CommonPage


class InvoiceListPage(CommonPage):
    # Locators

    # Lists Locators
    INVOICE_LIST_VALUES = (By.XPATH, '//div/div[@class="row"]')

    # Input Locators

    # Button Locators
    BUTTON_LOGOUT = (By.CSS_SELECTOR, 'a.btn-outline-info')
    BUTTON_INVOICE_DETAILS = (By.XPATH, './/div/a[contains(@href, "invoice")]')

    # Other Locators
    PAGE_TITLE = (By.CSS_SELECTOR, 'span.navbar-brand')
    PAGE_HEADER_TITLE = (By.CSS_SELECTOR, 'h2.mt-5')

    # Initializer

    def __init__(self, browser):
        super().__init__(browser)

    # Interaction Element Methods

    def get_page_title_value(self):
        value = self.browser.find_element(*self.PAGE_TITLE).text
        return value

    def get_page_header_title_value(self):
        element = self.wait_visibility_of_element_located(
            self.PAGE_HEADER_TITLE)
        value = element.text
        return value

    def btn_logout_click(self):
        self.wait_element_to_be_clickable(self.BUTTON_LOGOUT).click()

    # Interaction Action Methods

    def open_invoice_detail(self, item):
        # wait for the elements of the list to be clickable
        # and the button of the invoice detail to be clickable
        invoice_values = self.wait_visibility_of_all_elements_located(
            self.INVOICE_LIST_VALUES)
        self.wait_element_to_be_clickable(self.BUTTON_INVOICE_DETAILS)

        invoice_values[item -
                       1].find_element(*self.BUTTON_INVOICE_DETAILS).click()

        # Wait for the new window to open
        self.wait_for_new_window()
