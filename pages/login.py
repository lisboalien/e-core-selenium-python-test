"""
This module contains LoginPage,
the page object for the Login of automation-sandbox URL.
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from random import choice

from pages.common import CommonPage


class LoginPage(CommonPage):

    # URL

    URL = 'https://automation-sandbox.herokuapp.com'

    # Locators

    # Lists Locators

    # Input Locators
    TEXTBOX_USERNAME_INPUT = (By.NAME, 'username')
    TEXTBOX_PASSWORD_INPUT = (By.NAME, 'password')

    # Button Locators
    BUTTON_LOGIN = (By.ID, 'btnLogin')

    # Other Locators
    ALERT_DANGER_MESSAGE_LOCATOR = (By.CSS_SELECTOR, 'div.alert-danger')

    # Initializer

    def __init__(self, browser):
        super().__init__(browser)

    # Interaction Element Methods

    def load(self):
        self.browser.get(self.URL)

    def get_danger_message_value(self):
        value = self.browser.find_element(*self.ALERT_DANGER_MESSAGE_LOCATOR).text
        return value

    def set_username_input(self, username):
        self.browser.find_element(
            *self.TEXTBOX_USERNAME_INPUT).send_keys(username)

    def set_password_input(self, password):
        self.browser.find_element(
            *self.TEXTBOX_PASSWORD_INPUT).send_keys(password)

    def btn_login_click(self):
        self.browser.find_element(*self.BUTTON_LOGIN).click()

    # Interaction Action Methods
