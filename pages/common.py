"""
This module contains CommonPage,
the page object for the common methods to all the pages that are tested
"""
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class CommonPage:
    # Locators

    # Initializer

    def __init__(self, browser):
        self.browser = browser

    # Waits
    def wait_visibility_of_element_located(self, element_locator):
        wait = WebDriverWait(self.browser, 10)
        return wait.until(EC.visibility_of_element_located(element_locator))

    def wait_element_to_be_clickable(self, element_locator):
        wait = WebDriverWait(self.browser, 10)
        return wait.until(EC.element_to_be_clickable(element_locator))

    def wait_visibility_of_all_elements_located(self, list_locator):
        wait = WebDriverWait(self.browser, 10)
        element_list = wait.until(
            EC.visibility_of_all_elements_located(list_locator))
        return element_list

    def wait_url_contains(self, url_text):
        wait = WebDriverWait(self.browser, 10)
        return wait.until(EC.url_contains(url_text))

    def wait_for_new_window(self, timeout=10):
        handles_before = self.browser.window_handles
        yield
        WebDriverWait(self.browser, timeout).until(
            lambda browser: len(handles_before) != len(browser.window_handles))

    # Common Assertions Method
    def list_matches(self, phrase, term_list):
        matches = [t for t in term_list if phrase in t]
        return len(matches) > 0

    def find_webelement_on_list(self, element_text, term_list):
        matches = [
            t for t in term_list if element_text in t.get_attribute("innerText")]
        return matches[0]

    # Other methods
    def changing_tabs(self, window_to):
        window_after = self.browser.window_handles[window_to]
        self.browser.switch_to.window(window_after)
