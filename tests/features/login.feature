Feature: Login Automation Sandbox

  Background: Oppening Automation Sendbox page
    Given the Automation Sendbox page is displayed


  Scenario: Login (Positive)
    Given the user writes "demouser" as username
    And the user writes "abc123" as password
    When the user clicks on the Login button
    Then the system shows the Invoice List page


  Scenario Outline: Login (Negative)
    Given the user writes "<username>" as username
    And the user writes "<password>" as password
    When the user clicks on the Login button
    Then the system shows "Wrong username or password." message

    Examples:
      | username  | password |
      | Demouser  | abc123   |
      | demouser_ | xyz      |
      | demouser  | nananana |
      | demouser  | abc123   |

  Scenario: Validate invoice details
    Given the user is logs in with the credentials: "demouser" and "abc123"
    And the system shows the Invoice List page
    When the user clicks on Invoice Details button for the item 1
    Then the system shows the Invoice screen
    And all the item 1 data is displayed