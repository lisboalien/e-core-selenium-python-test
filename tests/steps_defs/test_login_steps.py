"""
This module contains step definitions for login.feature
"""
from pytest_bdd import scenarios, given, when, then, parsers

from pages.login import LoginPage
from pages.invoice_list import InvoiceListPage
from pages.invoice_page import InvoiceDetailPage
from pages.common import CommonPage


# Scenarios

scenarios('../features/login.feature')

# Given Steps


@given(parsers.parse('the user writes "{username}" as username'))
@given('the user writes "<username>" as username')
def step_input_username(browser, username):
    login_page = LoginPage(browser)
    login_page.set_username_input(username)


@given(parsers.parse('the user writes "{password}" as password'))
@given('the user writes "<password>" as password')
def step_input_password(browser, password):
    login_page = LoginPage(browser)
    login_page.set_password_input(password)


@given(parsers.parse('the user is logs in with the credentials: "{username}" and "{password}"'))
@given('the user is logs in with the credentials: "<username>" and "<password>"')
def step_login_credentials(browser, username, password):
    login_page = LoginPage(browser)
    login_page.set_username_input(username)
    login_page.set_password_input(password)
    login_page.btn_login_click()

# When Steps


@when('the user clicks on the Login button')
def step_login_button_click(browser):
    login_page = LoginPage(browser)
    login_page.btn_login_click()


@when(parsers.cfparse('the user clicks on Invoice Details button for the item {item:Number}', extra_types=dict(Number=int)))
@when('the user clicks on Invoice Details button for the item <item>')
def step_invoice_detail_click(browser, item):
    invoice_page = InvoiceListPage(browser)
    invoice_page.open_invoice_detail(item)

# Then Steps


@given('the system shows the Invoice List page')
@then('the system shows the Invoice List page')
def step_assert_invoice_list_page(browser):
    invoice_list_page = InvoiceListPage(browser)
    header_title = invoice_list_page.get_page_header_title_value()
    assert header_title == 'Invoice List'


@then(parsers.parse('the system shows "{message}" message'))
@then('the system shows "<message>" message')
def step_assert_alert_error_message(browser, message):
    login_page = LoginPage(browser)
    error_message = login_page.get_danger_message_value()
    assert error_message == message


@then('the system shows the Invoice screen')
def step_assert_invoice_item_page(browser, item):
    invoice_page = InvoiceDetailPage(browser)

    # Change the main tab to the new one (always the last value of the array)
    # and wait for the url to load
    invoice_page.changing_tabs(-1)
    invoice_page.wait_url_contains(
        "https://automation-sandbox.herokuapp.com/invoice")

    header_title = invoice_page.get_header_title()
    assert header_title == 'Invoice Details'


@then(parsers.cfparse('all the item {item:Number} data is displayed', extra_types=dict(Number=int)))
@then('all the item <item> data is displayed')
def step_assert_invoice_item_data_details(browser, item):
    invoice_page = InvoiceDetailPage(browser)
    invoice_page.assert_invoice_data(item)
